import 'package:flutter/material.dart';
import 'package:android_kiosk_mode/android_kiosk_mode.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: const Text('Android kiosk mode example'),
        ),
        body: Center(
            child: Column(
          children: <Widget>[
            FlatButton(
              child: Text('Check is device owner'),
              onPressed: () async {
                bool isDeviceOwner = await AndroidKioskMode.isDeviceOwner;
                scaffoldKey?.currentState?.showSnackBar(SnackBar(
                  content: Text('isDeviceOwner: $isDeviceOwner'),
                ));
              },
            ),
            FlatButton(
              child: Text(AndroidKioskMode.isEnabled ? 'Disable' : 'Enable'),
              onPressed: () async {
                if (AndroidKioskMode.isEnabled) {
                  await AndroidKioskMode.disable();
                } else {
                  await AndroidKioskMode.enable();
                }
                setState(() {});
              },
            ),
          ],
        )),
      ),
    );
  }
}
