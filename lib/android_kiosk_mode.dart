import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

class AndroidKioskMode {
  static const MethodChannel _channel =
      const MethodChannel('android_kiosk_mode');

  static bool isEnabled = false;

  static throwErrorIfNotAndroid() {
    if (!Platform.isAndroid) {
      throw UnsupportedError('This plugin only supports Android.');
    }
  }

  static Future<bool> get isDeviceOwner async {
    throwErrorIfNotAndroid();
    return await _channel.invokeMethod('isDeviceOwner');
  }

  static Future<bool> clearDeviceOwner() async {
    return await _channel.invokeMethod('clearDeviceOwner');
  }

  static Future<bool> enable() async {
    throwErrorIfNotAndroid();
    isEnabled = true;
    final result = await _channel.invokeMethod('on');
    return result;
  }

  static Future<bool> disable() async {
    throwErrorIfNotAndroid();
    isEnabled = false;
    final result = await _channel.invokeMethod('off');
    return result;
  }
}
