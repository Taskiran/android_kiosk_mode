#import "AndroidKioskModePlugin.h"
#import <android_kiosk_mode/android_kiosk_mode-Swift.h>

@implementation AndroidKioskModePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAndroidKioskModePlugin registerWithRegistrar:registrar];
}
@end
