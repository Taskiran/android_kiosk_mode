package com.mahmuttaskiran.android_kiosk_mode

import android.app.admin.DeviceAdminReceiver
import android.content.ComponentName
import android.content.Context
import android.util.Log

class MyDeviceAdminReceiver : DeviceAdminReceiver() {
  companion object {
    fun getComponentName(context: Context): ComponentName {
      val name =  ComponentName(context.applicationContext, MyDeviceAdminReceiver::class.java)
      Log.i("MyDeviceAdminReceiver", name.toString())
      return name
    }
  }
}