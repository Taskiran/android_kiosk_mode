package com.mahmuttaskiran.android_kiosk_mode

import android.app.Activity
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import java.lang.Exception


class AndroidKioskModePlugin(var activity: Activity) : MethodCallHandler {

  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "android_kiosk_mode")
      channel.setMethodCallHandler(AndroidKioskModePlugin(registrar.activity()))
    }
  }

  private var mAdminComponentName = MyDeviceAdminReceiver.getComponentName(activity.applicationContext)
  private var mDevicePolicyManager = activity.applicationContext.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager

  private val mFlags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
    (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        or View.SYSTEM_UI_FLAG_FULLSCREEN
        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
  } else {
    (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        or View.SYSTEM_UI_FLAG_FULLSCREEN)
  }

  private fun kioskModeOn() {
    // lock task
    lockTask(true)
    // set flags
    activity.window.decorView.systemUiVisibility = mFlags
    // set default application
    setDefaultApplication()
    // disable keyguard
    setKeyguardDisabled(true)
    // add keep on screen flag
    setKeepAwake(true)
    // enable stay awake
    setStayAwake(true)
  }

  private fun kioskModeOff() {
    // unlock task
    lockTask(false)
    // set flags
    activity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    // enable keyguard
    setKeyguardDisabled(false)
    // remove keep on screen flag
    setKeepAwake(false)
    // disable stay awake
    setStayAwake(false)
    // remove default application
    removeDefaultApplication()
  }

  private fun lockTask(enabled: Boolean) {
    if (!isDeviceOwner()) return
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      if (enabled) {
        mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, arrayOf(activity.application.packageName))
        activity.startLockTask()
      } else {
        activity.stopLockTask()
      }
    }
  }

  private fun setStayAwake(enabled: Boolean) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      if (enabled) {
        mDevicePolicyManager.setGlobalSetting(mAdminComponentName,
            Settings.Global.STAY_ON_WHILE_PLUGGED_IN,
            (BatteryManager.BATTERY_PLUGGED_AC
                or BatteryManager.BATTERY_PLUGGED_USB
                or BatteryManager.BATTERY_PLUGGED_WIRELESS).toString())
      } else {
        mDevicePolicyManager.setGlobalSetting(mAdminComponentName,
            Settings.Global.STAY_ON_WHILE_PLUGGED_IN, null)
      }
    }
  }

  private fun setKeepAwake(enabled: Boolean) {
    if (enabled) {
      activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    } else {
      activity.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }
  }

  private fun setKeyguardDisabled(disabled: Boolean) {
    if (isDeviceOwner()) return
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      mDevicePolicyManager.setKeyguardDisabled(mAdminComponentName, disabled)
    }
  }

  private fun getLauncherActivityName(): String {
    var activityName = ""
    val pm = activity.applicationContext.packageManager
    val intent = pm.getLaunchIntentForPackage(activity.applicationContext.packageName)
    val activityList = pm.queryIntentActivities(intent, 0)
    if (activityList != null) {
      activityName = activityList[0].activityInfo.name
    }
    return activityName
  }

  private fun setDefaultApplication() {
    if (!isDeviceOwner()) return
    val intentFilter = IntentFilter(Intent.ACTION_MAIN)
    intentFilter.addCategory(Intent.CATEGORY_HOME)
    intentFilter.addCategory(Intent.CATEGORY_DEFAULT)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      mDevicePolicyManager.addPersistentPreferredActivity(mAdminComponentName,
          intentFilter, ComponentName(activity.applicationContext.packageName, getLauncherActivityName()))
    }
  }


  private fun removeDefaultApplication(){
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      mDevicePolicyManager.clearPackagePersistentPreferredActivities(mAdminComponentName, activity.packageName)
    }
  }

  private fun isDeviceOwner(): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      return mDevicePolicyManager.isDeviceOwnerApp(activity.applicationContext.packageName)
    } else {
      false
    }
  }

  private fun clearDeviceOwner() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      mDevicePolicyManager.clearDeviceOwnerApp(activity.packageName)
    }
  }


  override fun onMethodCall(call: MethodCall, result: Result) {
    Log.i("AndroidKioskMode", "call: ${call.method}")
    when {
      call.method == "isDeviceOwner" -> {
        Log.i("AndroidKioskMode", "isDeviceOwner: ${isDeviceOwner()}")
        result.success(isDeviceOwner())
      }
      call.method == "on" -> {
        try {
          kioskModeOn()
          result.success(true)
        } catch (e: Exception) {
          result.success(false)
          Log.i("AndroidKioskMode", "err: $e ${e.stackTrace}")
        }
      }
      call.method == "clearDeviceOwner" -> {
        try {
          clearDeviceOwner()
          result.success(true)
        } catch (e: Exception) {
          result.success(false)
          Log.i("AndroidKioskMode", "err: $e ${e.stackTrace}")
        }
      }
      call.method == "off" -> {
        try {
          kioskModeOff()
          result.success(true)
        } catch (e: Exception) {
          result.success(false)
          Log.i("AndroidKioskMode", "err: $e ${e.stackTrace}")
        }
      }
      else -> result.notImplemented()
    }
  }
}
